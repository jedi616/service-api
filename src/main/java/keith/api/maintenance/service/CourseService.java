package keith.api.maintenance.service;

import java.util.Comparator;
import java.util.List;

import keith.domain.dto.CourseDto;
import keith.domain.dto.ServiceResponse;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface CourseService
{
    ServiceResponse<List<CourseDto>> retrieveAll() throws Exception;

    ServiceResponse<List<CourseDto>> retrieveAll(Comparator<CourseDto> comparator) throws Exception;

    ServiceResponse<CourseDto> retrieveNew() throws Exception;

    ServiceResponse<CourseDto> retrieveOne(long courseId) throws Exception;

    ServiceResponse<CourseDto> save(CourseDto courseDto) throws Exception;

    ServiceResponse<CourseDto> delete(long courseId) throws Exception;
}
