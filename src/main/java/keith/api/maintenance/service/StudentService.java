package keith.api.maintenance.service;

import java.util.Comparator;
import java.util.List;

import keith.domain.dto.ServiceResponse;
import keith.domain.dto.StudentDto;

/**
 * 2017/12/25:
 * The following are the details for all service layer public methods:
 *   - Should return instance of ServiceResponse<D>, where D is the Type/Class of the response's body.
 *   - Should have throws Exception to cascade-down all unexpected errors.
 * 
 * 2017/12/21:
 * All service layer public methods should return instance of ServiceResponse<D>, where D is the Type/Class of the response's body.
 * This also means these methods do not throw Exception as they are already handled/catch within, and Catch implementations should call ServiceResponse.setError(String message).
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentService
{
    /**
     * 
     * @return returns ServiceResponse containing List of Dto's.
     * @throws Exception
     */
    ServiceResponse<List<StudentDto>> retrieveAll() throws Exception;

    /**
     * 
     * @param comparator
     * @return returns ServiceResponse containing Sorted List of Dto's.
     * @throws Exception
     */
    ServiceResponse<List<StudentDto>> retrieveAll(Comparator<StudentDto> comparator) throws Exception;

    /**
     * 
     * @return returns new DTO instance for creation/adding.
     * @throws Exception
     */
    ServiceResponse<StudentDto> retrieveNew() throws Exception;

    /**
     * 
     * @param studentId
     * @return returns the DTO given the id.
     * @throws Exception
     */
    ServiceResponse<StudentDto> retrieveOne(long studentId) throws Exception;

    /**
     * <P>
     * Handles both insert & update.
     * 
     * @param Dto
     * @return returns ServiceResponse.
     * @throws Exception
     */
    ServiceResponse<StudentDto> save(StudentDto studentDto) throws Exception;

    /**
     * <P>
     * Deletes an entity given the Id.
     * 
     * @param id
     * @return returns ServiceResponse.
     * @throws Exception
     */
    ServiceResponse<StudentDto> delete(long studentId) throws Exception;
}
