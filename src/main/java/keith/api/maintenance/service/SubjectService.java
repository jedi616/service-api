package keith.api.maintenance.service;

import java.util.List;

import keith.domain.dto.ServiceResponse;
import keith.domain.dto.SubjectDto;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface SubjectService
{
    ServiceResponse<List<SubjectDto>> retrieveAll() throws Exception;

    ServiceResponse<SubjectDto> retrieveOne(long subjectId) throws Exception;

    ServiceResponse<SubjectDto> save(SubjectDto subjectDto) throws Exception;

    ServiceResponse<List<SubjectDto>> saveAll(List<SubjectDto> subjectDtos) throws Exception;

    ServiceResponse<SubjectDto> delete(long subjectId) throws Exception;
}
